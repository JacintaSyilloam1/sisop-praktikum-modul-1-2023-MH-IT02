#!/bin/bash

filename="users.txt"
auth_file="auth.log"

note_log() {
        local type="$1"
        local messages="$2"
        local timestamp=$(date "+[%d/%m/%y %H:%M:%S]")
        echo "$timestamp [$type] $message" >> "$auth_file"
        }

login() {
	read -p "Enter Email: " email
	read -s -p "Enter Password: " password
	echo

	#Cek email dan password
	if grep -q "^$email:" "$filename"; then
    		username=$(grep "^$email:" "$filename" | cut -d ':' -f 2)
		password_stored=$(grep "^$email:" "$filename" | cut -d ':' -f 3)

		encrypted_password=$(echo -n "$password_stored" | base64 -d)
		echo "Decrypted Password: $encrypted_password"

		if [[ "$password" == "$encrypted_password" ]]; then
    			echo "LOGIN SUCCESS - Welcome, $username"
    			note_log "LOGIN SUCCESS" "User $username logged in successfully"
		else
	         	 echo "LOGIN FAILED - Failed login attempt"
        	  	 note_log "LOGIN FAILED" "ERROR Failed login attempt with email $email"
       		fi

  	else
    		echo "LOGIN FAILED - Email $email not registered or incorrect password"
    		note_log "LOGIN FAILED" "ERROR Failed login attempt with email $email"
  	fi

	}

login
