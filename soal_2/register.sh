#!/bin/bash

filename="users.txt"
auth_file="auth.log"

note_log() {
	local type="$1"
	local messages="$2"
  	local timestamp=$(date "+[%d/%m/%y %H:%M:%S]")
  	echo "$timestamp [$type] $message" >> "$auth_file"
	}

register(){
	read -p "Enter your Email: " email

	#Cek email
        if grep -q "^$email" "$filename"; then
                echo "REGISTER FAILED - Email $email is already registered."
                note_log "REGISTER FAILED" "Email $email is already registered."
                exit 1
        fi
	read -p "Enter your Username: " username

	read -s -p "Enter your Password: " password
	echo

	# Cek password
  	if [ ${#password} -lt 8 ] || ! [[ "$password" =~ [A-Z] ]] || ! [[ "$password" =~ [a-z] ]] || \
     	! [[ "$password" =~ [0-9] ]] || [[ "$password" == *"$username"* ]]; then
    		echo "REGISTER FAILED - Password does not meet criteria."
    		note_log "REGISTER FAILED" "Password does not meet criteria for email $email."
    		exit 1
	fi

	# Cek apakah password memiliki setidaknya 1 simbol unik
	unique_symbols="!@#\$%^&*()_+{}\[\]:;<>,.?|\\"
	has_unique_symbols=false

	for ((i = 0; i < ${#unique_symbols}; i++)); do
  		char="${unique_symbols:$i:1}"
  	if [[ "$password" == *"$char"* ]]; then
    		has_unique_symbols=true
    	break
  	fi
	done

	if ! $has_unique_symbols; then
  		echo "REGISTER FAILED - Password must contain at least 1 special character."
  		note_log "REGISTER FAILED" "Password does not contain at least 1 special character."
  		exit 1
	fi

	#Encrypt password menggunakan base64
        encrypted_password(){
        echo -n "$1" | base64
        }

        encrypted_pass=$(encrypted_password "$password")

	#Simpan data register ke dalam filename
	echo "$email:$username:$encrypted_pass" >> "$filename"
	echo "REGISTER SUCCESS - Registration Success"

	#Simpan log ke auth.log
	note_log "REGISTER SUCCESS " "User $username registered successfully"

	}

register
