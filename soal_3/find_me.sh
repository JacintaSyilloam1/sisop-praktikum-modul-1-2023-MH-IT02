#!/bin/bash

gc=genshin_character

for file in "$gc"/*; do
	if [ -f "$file" ];then
		timestamp=$(date "+[%d/%m/%y %H:%M:%S]")
		image_path=$(realpath "$file")
		steghide extract -sf "$file" -p "" -f
		sleep 1
		found=$(cat *.txt | base64 -d 2>/dev/null | grep -i "https")
		
		if echo $found | grep -i "https"; then
			wget "$found"
			echo "$timestamp [FOUND] [$image_path]" >> image.log

			exit
		else
			echo "$timestamp [NOT FOUND] [$image_path]" >> image.log
			rm *.txt
		fi
	fi
done